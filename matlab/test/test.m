% +++++++++++++++++++++++++++++++++++++++
%
% MATLAB webMust library for rest-API usage
% 
% test file 
% for any information regarding the API https://bepicolombo.esac.esa.int/webclient-must/mustlink/api-docs/index.html#/Projects/getProjects
%
% +++++++++++++++++++++++++++++++++++
% Creator: Carmelo Magnafico IAPS/INAF
% date: 10/07/2019
%
% ++++++++++++++++++

clear all
close all
addpath('./classes/');
M = MatMust;

% for info type help "MatMust_login"
MatMust_login(M, jsondecode('{"username": "ajejebrazorv",  "password": "123456%!", "maxDuration": false}'));


% Ask for the Projects available to the user
proj_list = M.MatMust_getUserLinkedProjects('dump');
%%%
% with output:
% +++++++++++++ PROJECTS LIST +++++++++++++
% name: BEPITEST
% 	description: Test Database for Instruments  Team
% 	id: 8
% +++++++++++++++++	
% [...]
% +++++++++++++++++
% 	name: BEPICRUISE
% 	description: BepiColombo Cruise Phase
% 	id: 9
% +++++++++++++++++

act_proj = 'BEPICRUISE';

% for info type "help MatMust_getDataFromName"
% MatMust_getDataFromName(obj, ds, parname, dateStart, dateStop, options  )
data = M.MatMust_getDataFromName('BEPICRUISE', 'NSA03016', '2018-11-26 13:00:00', '2018-11-26 13:40:00');

% or if you want to plot 
data = M.MatMust_getDataFromName('BEPICRUISE', 'NSA03016', '2018-11-26 13:00:00', '2018-11-26 13:40:00', 'plot');

