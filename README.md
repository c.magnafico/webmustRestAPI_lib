# MatMust minimal Documentation

MatMust is a Matlab™ class to access to ESA WebMust via API.

Documentation of the ESA rest API is available [here](https://bepicolombo.esac.esa.int/webclient-must/mustlink/api-docs/index.html#/Projects/getProjects).

A comprehensive example is available in `/test` of this repository. Simply copy all the folder in your working directory and run test.

## Installation

To install simply copy the `src/classes` folder into your working directory and type in matlab `addpath('relativepathto_classes_dir')`

Than simply initialize the class with Mywebmustclass = MatMust. 

When the user will login via the login class method `MatMust_login()` a auth.json file with the authorization tocken will be wrote on your workingdirectory root.  

The tocken is valid for a 24h only. If the class see a expired tocken, a new login is requested to renew the authorization token to the API.

## List of Methods

The functions for this issues is only oriented to TM parameters read. The TC part is foreseen, but still to be wrote.

### MatMust_login

`MatMust_login(M, loginjson)`
inputs: 
- `M` -> A MutMust Class
- `loginjson` -> `jsondecode('{"username": "ajejebrazorv",  "password": "123456%!", "maxDuration": false}')` A json encoded struct that contains WebMust `username`, `password` and `maxDuration` option for the login.
Outputs:
none

When the user will succesfully login via the login class method `MatMust_login()` a auth.json file with the authorization tocken will be wrote on your workingdirectory root.  

### MatMust_logincheck

`t = MatMust_logincheck(M)`

A check method who ensure that you are actually logged in and your API tocken is still valid
inputs: 
- `M` -> A MutMust Class
outputs:
- `t` -> A boolean true if the user is logged, false if it is not.

### MatMust_getUserLinkedProjects

`projs = MatMust_getUserLinkedProjects(M, [option])`

List the projects available (also named **dataprovider**) for the current loggedin user of the Class. 

input:
- `M` -> A MutMust Class, succesfully logged in 
- `[option]` -> if `'dump'` is passed, the Projects will be printed at screen.

outputs:

- `projs` -> an array of strutcures containing all the User linked projects. es: projs -> id, name, description

```
projs(4) = 
  struct with fields:
             id: 9
           name: 'BEPICRUISE'
    description: 'BepiColombo Cruise Phase'
```

### MatMust_getDataFromName

Search a parameter into the requested **dataprovider** (es. 'BEPICRUISE')

`results = MatMust_searchTMparFromName(M, namequery, dataproviders, options)`

inputs:
- `M` -> A MutMust Class, succesfully logged in 
- `namequery` -> The query to perform (ex. '*HGA' )
- `dataproviders` -> dataprovider IdName. Can be more than one passed as a cell `{'BEPICRUISE','...'}`
- `options` ->  You can specify several options: 
    - 'dump' -> print to screen the parameters found
    - 'Description' -> Extend search in Parameters Descriptions
    - 'Id' -> Extend Search in Parameter Id
    - 'Unit' -> Extend Search in Unit name space

outputs:
 - `results` -> a struct array of resulting search in all the ds requested
                ```
                r = 
                struct with fields:
                        type: 'BEPICRUISE params'
                    typeId: 'params'
                    children: [2×1 struct]
                ```
es:
`r = MatMust_searchTMparFromName(M, 'APMH processed ', 'BEPICRUISE','dump','Description','title')`



### MatMust_getDataFromName
MatMust_getDataFromName get the data name parameter from the project name `ds` in the time interval `dateStart`, `dateStop`.

`data = MatMust_getDataFromName(M, ds, parname, dateStart, dateStop, options  )`

inputs:
- `M`  -> A MutMust Class, succesfully logged in 
- `ds` ->  'string' -> name of the dataprovider i.e.'BEPICRUISE'
- `parname` -> 'string' -> a parameter name i.e. 'NCAD7EB7' or a struct of strings {'NCAD7EB7', 'NCAD7EB8' ...}
- `dateStart` -> 'yyyy-mm-dd HH:MM:SS' format date UTC time
- `dateStop` -> 'yyyy-mm-dd HH:MM:SS' format date UTC time
- `options` ->  You can specify several options: 
    - `'dump'` will print on screen all the results
    - `'plot'` optional parameter to dump and plot the data
    - `'calibrated'` optional parameter that specify that you want calibrated data





